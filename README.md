## Chromium

Bastille App Template to bootstrap Chromium

## Add Devfs Rules to Allow Wayland DRM, Audio and HW ACC

```shell
echo -e "[devfsrules_hw_acc_jail=11] \nadd include \$devfsrules_hide_all \nadd include \$devfsrules_unhide_basic \nadd include \$devfsrules_unhide_login \nadd path dri unhide \nadd path 'dri/*' unhide \nadd path 'drm*' unhide \nadd path 'drm/*' unhide \nadd path 'nvidia*' unhide \nadd path 'mixer*' unhide \nadd path 'dsp*' unhide" >> /etc/defaults/devfs.rules 
```
## Create

```shell
bastille create TARGET 13.1-RELEASE IP 
```
## Bootstrap

```shell
bastille bootstrap https://gitlab.com/bastillebsd-apptemplates/chromium.git 
```
## Usage

```shell
bastille template TARGET https://gitlab.com/bastillebsd-apptemplates/chromium.git --arg XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR 
```

## Allow Xorg Jail to Host (exec like you user)

```shell
 xhost +
```
## Launch Chromium (WAYLAND)

```shell
 bastille cmd TARGET chromium wayland
```
## Launch Chromium (X11)

```shell
 bastille cmd TARGET chromium
```

## Screenshots (Wayland)

![Screenshot](https://gitlab.com/bastillebsd-apptemplates/chromium/-/raw/main/Screenshots/Wayland/20220929_02h48m39s_grim.png)
![Screenshot](https://gitlab.com/bastillebsd-apptemplates/chromium/-/raw/main/Screenshots/Wayland/20220929_02h49m04s_grim.png)
![Screenshot](https://gitlab.com/bastillebsd-apptemplates/chromium/-/raw/main/Screenshots/Wayland/20220929_02h49m50s_grim.png)

## Screenshots (X11)

![Screenshot](https://gitlab.com/bastillebsd-apptemplates/chromium/-/raw/main/Screenshots/X11/20220929_02h51m20s_grim.png)
![Screenshot](https://gitlab.com/bastillebsd-apptemplates/chromium/-/raw/main/Screenshots/X11/20220929_02h51m35s_grim.png)
![Screenshot](https://gitlab.com/bastillebsd-apptemplates/chromium/-/raw/main/Screenshots/X11/20220929_02h52m03s_grim.png)

